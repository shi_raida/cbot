/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
void setSpeed(int, int);
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define MOTOR_PRESCALER 2499
#define COUNTER_PERIOD 255
#define RPLIDAR_PRESCALER 4
#define MOTOR_RIGHT1_Pin GPIO_PIN_0
#define MOTOR_RIGHT1_GPIO_Port GPIOA
#define MOTOR_RIGHT2_Pin GPIO_PIN_1
#define MOTOR_RIGHT2_GPIO_Port GPIOA
#define MOTOR_LEFT1_Pin GPIO_PIN_6
#define MOTOR_LEFT1_GPIO_Port GPIOA
#define MOTOR_LEFT2_Pin GPIO_PIN_7
#define MOTOR_LEFT2_GPIO_Port GPIOA
#define MOTOR_CENTER1_Pin GPIO_PIN_0
#define MOTOR_CENTER1_GPIO_Port GPIOB
#define MOTOR_CENTER2_Pin GPIO_PIN_1
#define MOTOR_CENTER2_GPIO_Port GPIOB
#define RPLIDAR_MOTOR_Pin GPIO_PIN_12
#define RPLIDAR_MOTOR_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
/* Motors ---------------------------------------*/
#define MOTOR_RIGHT_ID 1
#define MOTOR_CENTER_ID 2
#define MOTOR_LEFT_ID 3
#define MOTOR_RIGHT_TIM &htim2
#define MOTOR_CENTER_TIM &htim3
#define MOTOR_LEFT_TIM &htim3
#define MOTOR_RIGHT1_CHANNEL TIM_CHANNEL_1
#define MOTOR_RIGHT2_CHANNEL TIM_CHANNEL_2
#define MOTOR_CENTER1_CHANNEL TIM_CHANNEL_3
#define MOTOR_CENTER2_CHANNEL TIM_CHANNEL_4
#define MOTOR_LEFT1_CHANNEL TIM_CHANNEL_1
#define MOTOR_LEFT2_CHANNEL TIM_CHANNEL_2
#define MOTOR_MAX_SPEED 256

/* RPLIDAR --------------------------------------*/
#define RPLIDAR_ID 4
#define RPLIDAR_TIM &htim10
#define RPLIDAR_CHANNEL TIM_CHANNEL_1
#define RPLIDAR_MAX_SPEED 153
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
